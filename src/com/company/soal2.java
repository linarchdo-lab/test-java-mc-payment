package com.company;

import java.util.ArrayList;
import java.util.Arrays;

public class soal2 {
    public static int[] myMethod(int[] Nums, int x) {
        ArrayList<Integer> num = new ArrayList<Integer>();
        for (int i = 0; i < Nums.length; i++) {
            boolean iscorrect = true;
            for(int j = 0; j < Nums.length; j++){
                if(Nums[j] != 0){
                    if(Nums[i] / Nums[j] == x && Nums[i]%Nums[j] == 0){
                        iscorrect = false;
                    }
                }
            }
            if(iscorrect){
                num.add(Nums[i]);
            }
        }
        int[] add = new int[num.size()];
        for(int i = 0; i < num.size(); i++){
            add[i] = num.get(i);
        }
        return add;
    }

    public static void main(String args[]) {
        int Nums[] = {0,0};
        int x = 0;
        System.out.println(Arrays.toString(myMethod(Nums, x)));
    }
}
