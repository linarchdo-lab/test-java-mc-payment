package com.company;

import java.util.ArrayList;
import java.util.Arrays;

public class soal3 {
    public static String[] myMethod(String word, int x) {
        String[] words = word.split(" ");
        ArrayList<String> tmp = new ArrayList<String>();

        for(int i = 0; i < words.length; i++){
            if(words[i].length() == x){
                tmp.add(words[i]);
            }
        }
        String[] newWords = new String[tmp.size()];
        for(int i = 0; i < tmp.size(); i++){
            newWords[i] = tmp.get(i);
        }
        return newWords;
    }

    public static void main(String args[]) {
        String word = "a b c f e r";
        int x = 0;
        System.out.println(Arrays.toString(myMethod(word, x)));
    }
}
